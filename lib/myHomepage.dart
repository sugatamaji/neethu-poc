import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterprovidertestapp/model.dart';
import 'package:flutterprovidertestapp/new_product.dart';
import 'package:provider/provider.dart';

//class MyTextFieldApp extends StatelessWidget {
//  ValueNotifier<bool> _textHasErrorNotifier = ValueNotifier(false);
//  TextEditingController myController = TextEditingController();
//
//  _updateErrorText(String text) {
//    var result = (text == null || text == "");
//    _textHasErrorNotifier.value = result;
//  }
//  _savedata(String text) {
//    if(text != null && text != "")
//    var results = myController.text;
//        myController.clear();
//
//
//  }
//  @override
//
//  Widget _getPrefixText() {
//    return Icon(Icons.ac_unit);
//  }
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//        debugShowCheckedModeBanner: false,
//        home: SafeArea(
//          child: Scaffold(
//              backgroundColor: Colors.white,
//              body: Container(
//                  padding: EdgeInsets.all(24.0),
//                  child: Center(
//                    child: ValueListenableBuilder(
//                      valueListenable: _textHasErrorNotifier,
//                      child: _getPrefixText(),
//                      builder: (BuildContext context, bool hasError, Widget child) {
//                        return
//                          Column(
//                            children: <Widget>[
//                              TextField(
//                                controller:myController ,
//                              onChanged: _updateErrorText,
//                              decoration: InputDecoration(
//                                prefix: child,
//                                fillColor: Colors.grey[100],
//                                filled: true,
//                                errorText: hasError ? 'Invalid value entered...' : null,
//                                enabledBorder: OutlineInputBorder(
//                                  borderSide: BorderSide(
//                                    color: Colors.grey,
//                                  ),
//                                  borderRadius: BorderRadius.circular(6.0),
//                                ),
//                                focusedBorder: OutlineInputBorder(
//                                  borderSide:
//                                  BorderSide(color: Colors.blueAccent, width: 0.0),
//                                  borderRadius: BorderRadius.circular(6.0),
//                                ),
//                                errorBorder: OutlineInputBorder(
//                                  borderSide: BorderSide(color: Colors.red, width: 0.0),
//                                  borderRadius: BorderRadius.circular(6.0),
//                                ),
//                                focusedErrorBorder: OutlineInputBorder(
//                                  borderSide: BorderSide(color: Colors.red, width: 0.0),
//                                  borderRadius: BorderRadius.circular(6.0),
//                                ),
//                              ),
//                        ),
//                              FloatingActionButton(
//                                // When the user presses the button, show an alert dialog containing
//                                // the text that the user has entered into the text field.
//                                onPressed: () {
//
//                                  return showDialog(
//                                    context: context,
//                                    builder: (context) {
//                                      return AlertDialog(
//                                        // Retrieve the text the that user has entered by using the
//                                        // TextEditingController.
//                                        content: Text(myController.text),
//                                      );
//                                    },
//                                  );
//                                },
//                                tooltip: 'Show me the value!',
//                                child: Icon(Icons.text_fields),
//                              ),
//                            ],
//                          );
//                      },
//                    ),
//                  )
//              ),
//
//
//          ),
//        )
//    );
//  }
//}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {

    final customerList = Provider.of<CustomerList>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
        itemCount: customerList.getCustomers().length,
        itemBuilder: (context, index) {
          return customerList.getCustomers()[index].name!=null?ListTile(
            title:customerList.getCustomers()[index].name!=null?
    Text('${customerList.getCustomers()[index].name}'):Text('nodata'),

            trailing: Container(
              width: 50,
              child: Row(
                children: <Widget>[
              IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        // removed customer
                        customerList.removeCustomer(index);
                      },

                  )
                ],
              ),
            ),
          ):SizedBox();
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Navigated to new customer page and passed object of CustomerList so that page can change data of customer list.
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => NewProduct(customerList: customerList)),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}