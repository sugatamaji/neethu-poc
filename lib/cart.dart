import 'package:flutter/material.dart';
import 'package:flutterprovidertestapp/myHomepage.dart';
import 'package:flutterprovidertestapp/model.dart';
import 'package:flutterprovidertestapp/model.dart';
import 'package:flutterprovidertestapp/productDetails.dart';
import 'package:provider/provider.dart';

class Cart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var itemList = Provider.of<CustomerList>(context);
   List<Mycart> value=[
     Mycart(
         id: 1,
         name: "Nike",
         image:"https://rukminim1.flixcart.com/image/832/832/jao8uq80/shoe/3/r/q/sm323-9-sparx-white-original-imaezvxwmp6qz6tg.jpeg?q=70",
         price: "250",
         shop_id: 2

     ),
     Mycart(
         id: 1,
         name: "Brasher Traveller",

         image:
         "https://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/merrell_0.jpg?itok=wFRPiIPw",
         price: "300",
         shop_id: 2

     ),
     Mycart(
         id: 1,
         name: "Nike",
         image: "https://n4.sdlcdn.com/imgs/d/h/i/Asian-Gray-Running-Shoes-SDL691594953-1-2127d.jpg",
         price: "280",
         shop_id: 2

     ),
     Mycart(
         id: 1,
         name: "Nike",
         image: "https://cdn.pixabay.com/photo/2014/06/18/18/42/running-shoe-371625_960_720.jpg",
         price: '500',
         shop_id: 2

     ),
   ];
    print('datassss---${value.length}');
    return Scaffold(
        appBar: new AppBar(
      actions: <Widget>[
        new Padding(
          padding: const EdgeInsets.all(10.0),
          child: new Container(
              height: 150.0,
              width: 30.0,
              child: new GestureDetector(

                child: new Stack(
                  children: <Widget>[
                    new IconButton(
                      icon: new Icon(
                        Icons.shopping_cart,
                        color: Colors.white,
                      ),
                      onPressed: null,
                    ),
                    itemList.cartIndex == 0
                        ? new Container()
                        : new Positioned(
                            child: new Stack(
                            children: <Widget>[
                              new Icon(Icons.brightness_1,
                                  size: 20.0, color: Colors.green[800]),
                              new Positioned(
                                  top: 3.0,
                                  right: 4.0,
                                  child: new Center(
                                    child: new Text(
                                      itemList.cartIndex.toString(),
                                      style: new TextStyle(
                                          color: Colors.white,
                                          fontSize: 11.0,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  )),
                            ],
                          )),
                  ],
                ),
              )),
        )
      ],
    ),
    body:  Container(


      child:ListView.builder(
    shrinkWrap: true,
    physics: ClampingScrollPhysics(),
    itemCount: value.length,
    itemBuilder: (context, index) {
      return Padding(
        padding: const EdgeInsets.only(top: 10,bottom: 10),
        child: GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProductDetail(itemList.cartIndex,value[index])),
            );
          },
          child: Card(
            child: Container(

              height: 150,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width*0.4,
                     height: MediaQuery.of(context).size.height*0.1752,
                        child:Image.network(value[index].image,fit: BoxFit.contain,) ,
                      ),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(value[index].name),
                            SizedBox(height: 5,),
                            Text("Price: ${value[index].price}"),
                            SizedBox(height: 5,),
                            Row(
                              children: <Widget>[
                            RaisedButton(
                            color:Colors.indigo ,
                              onPressed: () {
//                              itemList.addToCart(value[index]);
                                itemList.addToCart(index);

                              },
                              child: Text("Add to Cart",
                              style: TextStyle(color: Colors.white),),
                            ),
                                SizedBox(width: 10,),
                                RaisedButton(
                                  color:Colors.red ,
                                  onPressed: () {

                                    itemList.clear(index);

                                  },
                                  child: Text("Remove",
                                      style: TextStyle(color: Colors.white),),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),



            ),
          ),
        ),
      );
    }
      )
    ),);
  }
}
