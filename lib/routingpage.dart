import 'package:flutter/material.dart';
import 'package:flutterprovidertestapp/Hard/provider_stream.dart';
import 'package:flutterprovidertestapp/cart.dart';
import 'package:flutterprovidertestapp/myHomepage.dart';
import 'package:flutterprovidertestapp/model.dart';
import 'package:provider/provider.dart';
class RoutePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        title:
        Center(child: Text('Main Page')),

      ),
      body: SafeArea(child:

      Center(
        child: Column(

          children: <Widget>[
            SizedBox(height: MediaQuery.of(context).size.height*0.18,),
            ChangeNotifierProvider<CustomerList>(
              create: (context) => CustomerList(
                  myData: [
                    Mydata(),
                  ]
              ),
              child:     RaisedButton(
                onPressed: () {print('Button Clicked');
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MyHomePage(title: 'Add product')),
                );
                },
                child: Text('Easy'),
                color: Colors.indigo,
                textColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                ),
                padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
              ),
            ),

SizedBox(height: MediaQuery.of(context).size.height*0.01,),
            RaisedButton(
              onPressed: () {print('Button Clicked');
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Cart()),
              );},
              child: Text('Medium'),
              color: Colors.indigo,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(18.0),
              ),
              padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
            ),
            SizedBox(height: MediaQuery.of(context).size.height*0.01,),
            RaisedButton(
              onPressed: () {print('Button Clicked');
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProviderStream()),
              );},
              child: Text('Hard'),
              color: Colors.indigo,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(18.0),
              ),
              padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
            )
          ],
        ),
      )),
    );
  }
}
