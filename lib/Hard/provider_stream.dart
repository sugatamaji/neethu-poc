import 'package:flutter/material.dart';
import 'package:flutterprovidertestapp/productmodel.dart';

import 'package:provider/provider.dart';

class ProviderStream extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
//    Products products = Products();
    final productlists = Provider.of<Products>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Products List'),
        actions: <Widget>[
          new Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Container(
              height: 150.0,
              width: 30.0,
              child: new Stack(
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ),
                    onPressed: null,
                  ),
                  new Positioned(
                      child: new Stack(
                    children: <Widget>[
                      new Icon(Icons.brightness_1,
                          size: 20.0, color: Colors.green[800]),
                      new Positioned(
                          top: 3.0,
                          right: 4.0,
                          child: new Center(
                            child: StreamBuilder<int>(
                                stream: productlists.productCounter,
                                builder: (context, snapshot) {
                                  return new Text(
                                    (snapshot.data ?? 0).toString(),
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.w500),
                                  );
                                }),
                          )),
                    ],
                  )),
                ],
              ),
            ),
          )
        ],
      ),
      body: StreamBuilder<List<String>>(
        stream: productlists.productListNow,
        builder: (context, snapshot) {
          List<String> product = snapshot.data;
          return ListView.separated(
              separatorBuilder: (context, index) => Divider(),
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(product[index]),
                );
              });
        },
      ),

//      body: StreamBuilder <List<String>>(
////        stream:itemList,
//      ),
    );
  }
}
