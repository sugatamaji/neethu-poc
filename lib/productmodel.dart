import 'dart:async';

import 'package:flutter/foundation.dart';

List<String> product=["Product 1","Product 2","Product 3","Product 4","Product 5"];

class Products extends ChangeNotifier{
  Stream<List<String>> get productListNow async*{
for(var i=0;i<product.length;i++){
  await Future.delayed(Duration(seconds:2));
  yield product.sublist(0,i+1);
}
  }

  final StreamController<int> _productCounter=StreamController<int>();
  Stream<int> get productCounter=>_productCounter.stream;
  Products(){
    productListNow.listen((list) =>_productCounter.add(list.length));
  }
}