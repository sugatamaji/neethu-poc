import 'package:flutter/material.dart';
import 'package:flutterprovidertestapp/myHomepage.dart';
import 'package:flutterprovidertestapp/model.dart';
import 'package:flutterprovidertestapp/productmodel.dart';
import 'package:flutterprovidertestapp/routingpage.dart';

import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MultiProvider( //                                     <--- MultiProvider
        providers: [
    ChangeNotifierProvider<CustomerList>(create: (context) => CustomerList(
        myData: [
          Mydata(),
        ]
    )),
    ChangeNotifierProvider<Products>(create: (context) => Products()),
    ],


      child:MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: RoutePage()
      ),
    );
  }
}