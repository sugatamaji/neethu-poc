import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterprovidertestapp/model.dart';
import 'package:provider/provider.dart';


class NewProduct extends StatefulWidget {
  final customerList; // stores object of listener passed from calling class
  NewProduct({Key key, this.customerList}) : super(key: key);

  @override
  _NewProductState createState() => _NewProductState();
}

class _NewProductState extends State<NewProduct> {
  final GlobalKey<FormState> _formStateKey = GlobalKey<FormState>();
  String _name;
  String _age;

  final _nameController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New Product"),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            Form(
              key: _formStateKey,
              autovalidate: true,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, bottom: 5),
                    child: TextFormField(
                      onSaved: (value) {
                        _name = value;
                      },
                      controller: _nameController,
                      decoration: InputDecoration(
                        focusedBorder: new UnderlineInputBorder(
                            borderSide: new BorderSide(
                              width: 2,
                              style: BorderStyle.solid,
                            )),
                        labelText: "Product Name",
                        icon: Icon(Icons.account_box, color: Colors.grey),
                        fillColor: Colors.white,
                        labelStyle: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),

                ],
              ),
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
              RaisedButton(
                    color: Colors.green,
                    child: Text(
                      ('SAVE'),
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      _formStateKey.currentState.save();
                      // widget : is used to access property of parent stateful class
                      widget.customerList.addCustomer(
                          Mydata(name: _name));
                      Navigator.of(context).pop();
                    },
                  )

              ],
            )
          ],
        ),
      ),
    );
  }
}