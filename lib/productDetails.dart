import 'package:flutter/material.dart';
import 'package:flutterprovidertestapp/model.dart';
import 'package:provider/provider.dart';
class ProductDetail extends StatelessWidget {
  int cartIndex;
  Mycart mycart;
  ProductDetail(this.cartIndex,this.mycart);



  @override
  Widget build(BuildContext context) {
    bool additem=false;
    var itemList = Provider.of<CustomerList>(context,listen: false);
    return Scaffold(
      appBar: new AppBar(
        actions: <Widget>[
          new Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Container(
                height: 150.0,
                width: 30.0,
                child: new GestureDetector(
                  onTap: () {
//                  Navigator.of(context).push(new MaterialPageRoute(
//                      builder: (BuildContext context) =>
//                          new CartItemsScreen())
//                  );
                  },
                  child: new Stack(
                    children: <Widget>[
                      new IconButton(
                        icon: new Icon(
                          Icons.shopping_cart,
                          color: Colors.white,
                        ),
                        onPressed: null,
                      ),
                      cartIndex == 0
                          ? new Container()
                          : new Positioned(
                          child: new Stack(
                            children: <Widget>[
                              new Icon(Icons.brightness_1,
                                  size: 20.0, color: Colors.green[800]),
                              new Positioned(
                                  top: 3.0,
                                  right: 4.0,
                                  child: new Center(
                                    child:
                                    Consumer<CustomerList>(builder: (context,itemList,child){
                                    return  new Text(
//                                        additem?itemList.cartIndex:
                                        itemList.cartIndex.toString(),
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.w500),
                                  );})


                                  )),
                            ],
                          )),
                    ],
                  ),
                )),
          )
        ],
      ),
      body: SafeArea(
        child:Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 100,),
              child: Container(
                width: MediaQuery.of(context).size.width*1,
                height: MediaQuery.of(context).size.height*0.2,
                child:Image.network(mycart.image,fit: BoxFit.contain,) ,
              ),
            ),
            SizedBox(height: 30,),
            Text(mycart.name,style: TextStyle(fontSize: 20),),
            SizedBox(height: 10,),
            Text("Price: ${mycart.price}",style: TextStyle(fontSize: 15),),
            SizedBox(height: 20,),
            Consumer<CustomerList>(
              builder: (context,itemList,child){
                return     RaisedButton(
                  color:Colors.indigo ,
                  onPressed: () {
//                              itemList.addToCart(value[index]);
                    itemList.addToCart(cartIndex);
                    additem=true;
                  },
                  child: Text("Add to Cart",
                    style: TextStyle(color: Colors.white),),
                );
              }
            )

          ],
        ) ,
      ),
    );
  }
}
